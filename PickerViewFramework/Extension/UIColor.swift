//
//  UIColor.swift
//  localAuthendication
//
//  Created by Vijay on 12/07/19.
//  Copyright © 2019 Vijay. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    ///  Create Random color code for each time load different image color
    public static var randomColor : UIColor {
        return UIColor(red: .random(in: 0...1), green: .random(in: 0...1), blue: .random(in: 0...1), alpha: 1.0)
    }
    
    /// Return UIColor for hex string
    ///
    /// - Parameter hex: hex string
    /// - Returns: UIColor code
    public static func hexToUI(hex:String) -> UIColor {
        var cString = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if cString.hasPrefix("#"){
            cString.remove(at: cString.startIndex)
        }
        
        if cString.count != 6 {
            return UIColor.clear
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(red:CGFloat((rgbValue & 0xFF0000) >> 16)/255.0, green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0, blue: CGFloat(rgbValue & 0x0000FF) / 255.0, alpha: 1.0)
    }
}


// USAGES
/*
 Create Dynamic color from UIColor use of random(in:0...1) use to fetch RGB combinations
 
 static var randomColor : UIColor
 Variable use to create random color
 
 Example:
 let view = UIView(frame: self.bounds)
 view.backgroundColor = UIColor.randomColor
 self.addSubView(view)
 
 //Display the each and every time load different color
 
 */
