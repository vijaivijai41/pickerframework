//
//  UIViewController.swift
//  FICommon
//
//  Created by Sasikumar JP on 06/07/19.
//  Copyright © 2019 FundsIndia. All rights reserved.
//

import UIKit

extension UIViewController {
    
    /// Is the app running in split screen mode on iPad with
    /// iPhone traitCollection
    ///
    /// Returns - true / false
    public func isPhoneModeOnPad() -> Bool {
        return UIDevice.isPad() && traitCollection.horizontalSizeClass == .compact
    }
    
    
    public enum AlertStatus {
        case error
        case information
        
    }
    
    public func showAlert(for title:String? ,message: String?,type:AlertStatus ,okButtonAction: (()->())? ,cancelButtonAction: (() -> ())?) {
        let alerController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction.init(title: "Ok", style: .default) { ( _ ) in
            okButtonAction?()
        }
        if type == .error {
            let cancel = UIAlertAction.init(title: "cancel", style: .cancel) { ( _ ) in
                cancelButtonAction?()
            }
            alerController.addAction(cancel)
        }
        
        alerController.addAction(ok)
        
        UIApplication.topViewController()?.present(alerController, animated: true, completion: nil)
    }
    
}


// USAGES
/*
 Fetch the boolean status for splited device screen iPad
 
 public func isPhoneModeOnPad() -> Bool

 status of splite Display
 
 Example:
 if isPhoneModeOnPad {
    print("screen splited")
 }
 
 */
