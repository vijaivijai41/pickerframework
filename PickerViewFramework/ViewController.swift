//
//  ViewController.swift
//  PickerViewFramework
//
//  Created by vijayakumar on 27/09/19.
//  Copyright © 2019 sample. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var textfield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }

    @IBAction func showAction(_ sender: UIButton) {
        let datasource = FIPickerViewDataSource.singleElement(array: ["1","2","3"], title: "Numbers")
        
        _  = FIPickerView.showPicker(dataSource: datasource, selectedIndex: 0, orgin: sender, doneHandler: { (isDate, obj, indexes) in
            print(obj)
            print(indexes)
          print("done action")
            
        }, cancelHandler: {
            
        })
    }
    

}


